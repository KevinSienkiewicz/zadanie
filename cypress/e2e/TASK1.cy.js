describe('Login test', () => {
  beforeEach(() => {
    cy.visit('localhost:3000')
  })
  it('Login with correct data', () => {
    cy.get('#username').type('jan.testowy@wskz.pl')
    cy.get('#password').type('aqLrvDJ348')
    cy.get('[type="submit"]').click()
    //jan.testowy@wskz.pl 
    //password: aqLrvDJ348
    cy.get('body').contains('Welcome back')
  })
  it('Login with incorrect data', () => {
    cy.get('#username').type('NIEPOPRAWNEjan.testowy@wskz.pl')
    cy.get('#password').type('NIEPOPRAWNEaqLrvDJ348')
    cy.get('[type="submit"]').click()
    //NIEPOPRAWNEjan.testowy@wskz.pl 
    //NIEPOPRAWNEpassword: aqLrvDJ348
    cy.get('body').contains('Incorrect Username and/or Password!')
  })
})