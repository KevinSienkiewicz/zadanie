describe('Registration test', () => {
  beforeEach(() => {
    cy.visit('localhost:3000/registration')
  })
  it('passes', () => {
    cy.get('[data-cy="username"]').type('test.testowy@wskz.pl')
    cy.get('[data-cy="password"]').type('testpassword')
    cy.get('[data-cy="country"]').select('Poland')
    cy.get('[data-cy="hobby"]').select('Books')
    cy.get('#photo').selectFile('Test_Photo.jpg')
    cy.get('[data-cy="info"]').type('dodatkowe informacje')
    cy.get(':nth-child(7) > [data-cy="registration-consent"]').check()
    cy.get(':nth-child(8) > [data-cy="registration-consent"]').check()
    cy.get(':nth-child(9) > [data-cy="registration-consent"]').check()
    cy.get('.btn').click()
    cy.get('body').contains('Account created for')
  })
})